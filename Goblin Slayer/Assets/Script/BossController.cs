using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BossController : MonoBehaviour
{
    public float Speed = 0.4f;
    private int HP = 3;
    private float SpawnDelay = 0;
    public float TimeToThrow = 2;
    private bool DeathCheck = false;
    private bool HitCheck = false;
    private bool AttackCheck = false;
    private bool ScoreCheck = false;

    public AudioSource DyingSound;
    public GameObject Blood;
    private GameObject BloodClone;
    public GameObject Shadow;
    public Animator Anim;
    public GameObject Spear;
    private SpriteRenderer Sprite;
    private UIcontroller UI;
    // Start is called before the first frame update
    void Start()
    {
        UI = FindObjectOfType<UIcontroller>();
        Sprite = GetComponent<SpriteRenderer>();
        Anim = GetComponent<Animator>();
        DyingSound = GetComponent<AudioSource>();
      
    }

    // Update is called once per frame
    void Update()
    {
        if(HitCheck == false && DeathCheck == false && AttackCheck == false)
            transform.position -= new Vector3(Speed * Time.deltaTime, 0, 0);
        
        SpawnDelay += Time.deltaTime;
        if (SpawnDelay >= TimeToThrow )
        {
            StartCoroutine(Shot());
            StopCoroutine(Shot());
            SpawnDelay = 0;
        }
            

      
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            
            StartCoroutine(Hit());
            StopCoroutine(Hit());
            HP--;


            if (HP <= 0 && ScoreCheck == false)
            {
                StartCoroutine(Death());
                UI.ScoreUp();
                ScoreCheck = true;
                Shadow.transform.position = new Vector3(transform.position.x , Shadow.transform.position.y + 0.2f, 0);
            }
            
        }


        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Finish")
            SceneManager.LoadScene("GameOverDead");

    }

    IEnumerator Hit()
    {
        Anim.SetBool("Hit", true);
        HitCheck = true;
        gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        Anim.SetBool("Hit", false);
        gameObject.GetComponent<Collider2D>().enabled = true;
        HitCheck = false;

    }

    IEnumerator Death()
    {
        Anim.SetBool("NoMoreHP", true);
        DeathCheck = true;
        DyingSound.Play();
        BloodClone = Instantiate(Blood, new Vector3(transform.position.x, transform.position.y -0.4f, 0), Blood.transform.rotation);
        gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
        Anim.SetBool("NoMoreHP", false);
        Destroy(BloodClone);

    }

    IEnumerator Shot()
    {
        Anim.SetBool("Attacking", true);
        AttackCheck = true;
        yield return new WaitForSeconds(1);

        if(Anim.GetBool("Hit") == false && Anim.GetBool("NoMoreHP") == false)
            Instantiate(Spear, transform.position + new Vector3(-1, 0, 0), Spear.transform.rotation);
        Anim.SetBool("Attacking", false);
        AttackCheck = false;
    }

    
}
