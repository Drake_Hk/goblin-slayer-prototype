using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    public float Speed = 1;
    private bool DeathCheck = false;

    public AudioSource Scream;
    public GameObject Blood;
    private GameObject BloodClone;
    public GameObject Shadow;
    public Animator Anim;
    private UIcontroller UI;
    // Start is called before the first frame update
    void Start()
    {
        UI = FindObjectOfType<UIcontroller>();
        Anim = GetComponent<Animator>();
        Scream = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if( DeathCheck == false)
            transform.position -= new Vector3(Speed * Time.deltaTime, 0, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {   
           
            UI.ScoreUp();
            StartCoroutine(Death());
            Shadow.transform.position = new Vector3(transform.position.x, Shadow.transform.position.y + 0.5f, 0);
            Shadow.transform.localScale = new Vector3(1.5f, -0.2f, 0);

        }


        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Finish")
            SceneManager.LoadScene("GameOverDead");

    }

    IEnumerator Death()
    {
        Anim.SetBool("Hit", true);
        DeathCheck = true;
        Scream.Play();
        BloodClone = Instantiate(Blood, transform.position, Blood.transform.rotation);
        gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
        Destroy(BloodClone);
       
        
    }


 
}
