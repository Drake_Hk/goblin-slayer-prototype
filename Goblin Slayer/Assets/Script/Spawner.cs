using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject Goblin;
    public GameObject Boss;
    public GameObject UpLimit;
    public GameObject LowLimit;

    public int WaveArmy = 10;  // Maximum number of enemies to spawn in a wave
    private bool SpeedUpCheck = true;  // Help to increase speed 1 time only per wave
    private int IncreaseArmy = 15;   // Offset to incremente the maximum number of enemies to spawn

    private float SpawnDelay = 0;   
    public float TimeToSpawn = 1;
    public bool WaveStart;

    public AudioSource Audio;
    public AudioClip Growl;
    public AudioClip Win;
    private UIcontroller UI;
    public Enemy IncreseSpeed;
    public BossController BossSpeed;
    private int EnemySpawnCounter = 0;
    // Start is called before the first frame update
    void Start()
    {
        UI = FindObjectOfType<UIcontroller>();
        Audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (UI.Score == WaveArmy )
        {
            SpawnDelay = 0;
            TimeToSpawn = 4;
            WaveStart = false;
            Audio.PlayOneShot(Win, 3);
            if (SpeedUpCheck)
            {
                IncreseSpeed.Speed += 0.5f;
                BossSpeed.Speed += 0.2f;
                SpeedUpCheck = false;
            }

            EnemySpawnCounter = WaveArmy;
            WaveArmy += IncreaseArmy;
            IncreaseArmy += 5;


        }
      


        SpawnDelay += Time.deltaTime;
        print(SpawnDelay);

        if (SpawnDelay >= TimeToSpawn && EnemySpawnCounter <= (WaveArmy -1) )
        {
                         
            if(Random.Range(1,21) % 11 == 0)
            {
                Instantiate(Boss, new Vector3(transform.position.x, Random.Range(UpLimit.transform.position.y, LowLimit.transform.position.y), 0), Boss.transform.rotation);
                Audio.PlayOneShot(Growl, 2);
            }
            else
            {
                Instantiate(Goblin, new Vector3(transform.position.x, Random.Range(UpLimit.transform.position.y, LowLimit.transform.position.y), 0), Goblin.transform.rotation);
                SpawnDelay = 0;
                TimeToSpawn = 1;
                WaveStart = true;
                SpeedUpCheck = true;
            }
            EnemySpawnCounter++;
        }

     

    
    }

   

}
