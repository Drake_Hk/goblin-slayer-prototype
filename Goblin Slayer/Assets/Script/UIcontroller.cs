using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIcontroller : MonoBehaviour
{
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI WaveCounter;
    public int Score;
    private int WaveNumber = 1;
    private bool WaveCheck = false;

    
    public Spawner SpawnCheck;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.text = ""+Score;
        
        if (SpawnCheck.TimeToSpawn == 4)
        {
            
            WaveCounter.text = "Wave " + WaveNumber + " Completed!";
            WaveCheck = true;
            WaveCounter.CrossFadeAlpha(1, 0.5f, false);
        }  
        else if(SpawnCheck.WaveStart == true && WaveCheck == true)
        {
            
            WaveCounter.CrossFadeAlpha(0, 0.5f, false);
            WaveNumber++;
            WaveCheck = false;
        }
                



    }

    public void ScoreUp()
    {
        Score++;
    }

    
}