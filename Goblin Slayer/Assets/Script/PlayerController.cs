using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float Speed;

    public GameObject TopLimit;
    public GameObject BotLimit;
    public GameObject Shadow;
    public AudioSource Cast;

    public Animator Anim;
    public GameObject Knife;

    
    [SerializeField] float ShotDelay;
    private float Timer;
    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey("up") || Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(0, Input.GetAxis("Vertical") * Time.deltaTime * Speed, 0);
            Anim.SetBool("MovingUp", true);
            Shadow.transform.position = new Vector3(Shadow.transform.position.x, transform.position.y - 1.4f, transform.position.z); 
        }
        else if (Input.GetKey("down") || Input.GetKey(KeyCode.S))
        {
            transform.position += new Vector3(0, Input.GetAxis("Vertical") * Time.deltaTime * Speed, 0);
            Anim.SetBool("MovingDown", true);
            Shadow.transform.position = new Vector3(Shadow.transform.position.x, transform.position.y - 1.4f, transform.position.z);
        }
        else
        {
            Anim.SetBool("MovingUp", false);
            Anim.SetBool("MovingDown", false);
            Shadow.transform.position = new Vector3(Shadow.transform.position.x, transform.position.y - 0.9f, transform.position.z);

        }




        if (transform.position.y >= TopLimit.transform.position.y)
            transform.position = new Vector3(transform.position.x, TopLimit.transform.position.y, 0);

        if (transform.position.y <= BotLimit.transform.position.y)
            transform.position = new Vector3(transform.position.x, BotLimit.transform.position.y, 0);

        Timer += Time.deltaTime;

        if (Input.GetKeyDown("space"))
        {
            if (Timer >= ShotDelay)
            {
                StartCoroutine(Shooting());
                StopCoroutine(Shooting());
                Timer = 0;
            }    
        }
            
                   
    }

    IEnumerator Shooting()
    {
        Anim.SetBool("Attacking", true);
        Cast.Play();
        yield return new WaitForSeconds(0.5f);
        Instantiate(Knife, transform.position + new Vector3(1,0.3f,0) , Knife.transform.rotation);
        Anim.SetBool("Attacking", false);
    }
}
