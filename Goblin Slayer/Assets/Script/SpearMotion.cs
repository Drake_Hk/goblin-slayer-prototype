using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpearMotion : MonoBehaviour
{
    [SerializeField] float Speed;
       
    private int TimeToLive = 2;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, TimeToLive);
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(Speed * Time.deltaTime, 0, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
            SceneManager.LoadScene("GameOverDead");
    }

   
}
