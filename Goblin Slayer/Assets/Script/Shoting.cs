using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoting : MonoBehaviour
{
    [SerializeField] float Speed;
    private bool HitCheck = false;

    public AudioSource Impact;
    public Animator Anim;
    private int TimeToLive = 2;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, TimeToLive);
        Anim = GetComponent<Animator>();
        Impact = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(HitCheck == false)
            transform.position += new Vector3(Speed * Time.deltaTime, 0, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            HitCheck = true;
            StartCoroutine(Explosion());
        }
    }

    IEnumerator Explosion()
    {
        Anim.SetBool("Hit", true);
        Impact.Play();
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
}
